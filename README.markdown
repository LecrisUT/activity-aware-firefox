<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: © 2021 Matija Šuklje <matija@suklje.name>
-->

# Activity-aware Firefox

[![REUSE status](https://api.reuse.software/badge/gitlab.com/hook/activity-aware-firefox)](https://api.reuse.software/info/gitlab.com/hook/activity-aware-firefox)

## Problem

[Firefox][] is a great and powerful web browser by Mozilla, and [Plasma][] is a great and powerful desktop by KDE.

The problem is that one of the most powerful features of Plasma – Activites – do not work well with Firefox.

There are several pain points in this combination out of the box, including:

- a link opened in one Activity could open in a Firefox window in a different Activity (sometimes even starting that extra Activity, if it was stopped)
- when resuming a stopped Activity with Firefox in it, it would sometimes spawn other Firefox windows that are not related to that Activity
- in general Firefox windows tend to move around Activities where you do not expect them to be

This small script tries to work around those limitations.

[firefox]: https://getfirefox.org
[plasma]: https://kde.org/plasma-desktop/


## Solution: Make Firefox aware of Activites

In short, using `activityfirefox` you can have a Firefox window (or several) in each Plasma Activity, and they will be bound to that Activity.

What `activityfirefox` does is to create a [Firefox Profile][profile] for the Activity (using its unique ID) it was ran in.

[profile]: https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data

Then every next time you run `activityfirefox` in that Plasma Activity, it will start the Firefox Profile associated with it.


# Installation

Save the `activityfirefox` script to somewhere in your `$PATH`.

In my case, I saved it to `~/.local/bin/activityfirefox` and have that folder in my `$PATH`.

E.g. in [Fish shell][fish], this is what I did:

[fish]: https://fishshell.com/

```fish
git clone https://gitlab.com/hook/activity-aware-firefox.git
mkdir -p ~/.local/bin/
cp activity-aware-firefox/activityfirefox ~/.local/bin/
set -U fish_user_paths ~/.local/bin/ $fish_user_paths
```

Next, make sure your desktop knows about this “new” browser, by giving it a `.desktop` file. For example copy the one provided in this repository:

```fish
mkdir -p ~/.local/share/applications/
cp activity-aware-firefox/activityfirefox.desktop ~/.local/share/applications/
```

And finally, make sure that Plasma uses your new ”Activity-aware Firefox” as the default browser:

- open _System Settings_
- either search for “browser” or navigate manually to: _Personalisation_ ↦ _Applications_ ↦ _Default Applications_
- and there under _Web Browser_ select the new entry called “Activity-aware Firefox”


# Running

From this point onward, just use the “Activity-aware Firefox” browser as your broser of choice and be happy about it :)


# Caveats

There are some caveats, of course:

The biggest annoyance is that due to the fact that [Firefox kills itself if it has been sleeping for a certain period of time][ff_stop] after receiving e.g. `SIGSTOP`, if you resume a stopped Activity, Firefox will start with the default profile. You need to close that window and simply start “Activity-aware Firefox” and it will come back as expected.


A limitation of this script is that add-ons, bookmarks, settings, etc. are _not_ copied (except for `chrome/`) – I suggest using [Firefox Sync][sync] to sync all of this between Firefox Profiles. This is is also needed if you want to move tabs between Profiles/Activities.

Syncing everything through Firefox Sync can take some time when you run the “Activity-aware Firefox” in a new Plasma Activity for the first time – depending on how much stuff you sync, of course.

[sync]: https://www.mozilla.org/en-US/firefox/sync/
[ff_stop]: https://searchfox.org/mozilla-central/source/dom/ipc/ContentParent.h#1490


# Specifics for different use-cases


## How to find your default Firefox Profile

Depending on your installation, your default Firefox Profile will either be in `~/.mozilla/firefox/????????.default-release/` or `~/.mozilla/firefox/????????.default/`.

You can find the exact folder by going through the menu _Help_ ↦ _More Troubleshooting Information_ and in the tab that pops up search for “Profile Directory”.


## How to clean up Firefox Profiles not used anymore

After you stop using some Plasma Activities and as such the Firefox Profiles, you may be left with Profiles staying around. You can use `firefox --ProfileManager` to manage your existing Profiles, including removing them.

If I find a safe way to clean up in an automated way, I will add it.


## Firefox Sync

If you want to make sure the new Firefox Profiles look similar to each other, you might want to 

With Firefox Sync you can decide (per Profile/device) which of the following you want to sync:

- bookmarks
- history
- open tabs – does not auto-sync tabs, but allows you to move tabs between Profiles (and devices)
- logins and passwords – uses Firefox Lockwise
- credit card info
- add-ons
- preferences – not all preferences are synced though, as some are deemed too specific to be safely synced

If you use Firefox Sync, it makes sense to rename each “device”, so you know which Profile is which. A good option is to include the Activity name and the machine name.

One way of achieving something similar locally would be to carefully copy the contents of the default profile’s folder into the new profile’s folder. But that is not advised, because there are many things that are profile-specific, so if you do that, things tend to break.


## Rename Firefox windows to match the Activities

If you want to more easily identify which Firefox window belongs to which Plasma Activity, you can use the [Window Titler add-on][wt] to change the names of Firefox windows.

[wt]: https://addons.mozilla.org/en-GB/firefox/addon/window-titler/

Apparently it should be possible to do this through the Desktop as well using [Native Messaging WebExtension][native_messaging], but that is beyond my skills.

[native_messaging]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging


## Tree Style Tab


### Hiding the tab row

If you use the [Tree Style Tab add-on][tst], you probably want to hide the tab row on the top.

[tst]: https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/

For your convenience, see `userChrome.css` in this repository and copy it to your default profile. If `chrome/` folder does not exist yet in your default profile, create it.

Although the `chrome/` folder is copied when you first run this script, since Firefox 57 user chrome is by default disabled. So even after everything syncs over Firefox Sync, you will need to enable user chrome yourself. You can do so, if you:

- open `about:config`
- confirm that you will be careful ;)
- search for `toolkit.legacyUserProfileCustomizations.stylesheets` and enable it
- restart Firefox, so that change applies

If you still see the top tab-bar, check which your default Profile is. This script assumes the default profile is `**.default-release`, but on certain installations that might be `**.default` instead. if that is the case, change the `DEF_PROF` variable definition accordingly in the script.


### Sending groups of tabs to a different Activity

If you want to send whole branches of tabs to a different Firefox Profile (in our case: a Firefox window in a different Plasma Activity), you should head out to `about:addons` ↦ _Tree Style Tab_ ↦ _Preferences_ ↦ _Context Menu_ ↦ _Send Tabs to Other Devices with the context menu via Firefox Sync_ and follow the instructions there.

You will need to do this for every Profile that you want to tab trees to and from, as unfortunately this add-on does not see the device names of Firefox Sync.


## Firefox Multi-Account Containers

If you use the [Multi-Account Containers][containers] add-on, it can sync its containers settings through Firefox Sync, but you need to trigger that through the add-on itself.

[containers]: https://addons.mozilla.org/en-GB/firefox/addon/multi-account-containers/


## Worldbrain’s Memex and other add-ons using local storage

Some more complex add-ons that make use of local storage – e.g. [Worldbrain’s Memex][memex] – cannot sync through Firefox Sync. You have to take that into account.

(At least for Memex, its team is working on a way to sync between devices separately.)

[memex]: https://getmemex.com/


## How to move a vast amount of tabs

If you already have a huge amount of tabs open that you want to migrate to your new Activity-aware Firefox Profiles, I suggest you use the [Bulk URL Opener add-on][bulk].

[bulk]: https://addons.mozilla.org/en-US/firefox/addon/bulkurlopener/


# Inspiration & further reading

This is where I pulled inspiration from to create this script.

- <https://yuenhoe.com/blog/2012/08/associating-firefox-profiles-with-kde-activities/>
- <https://cukic.co/2016/02/08/heavy-activities-setup/>
- <https://bbs.archlinux.org/viewtopic.php?id=137941>
- <https://adrian15sgd.wordpress.com/2012/12/16/soporte-de-actividades-para-firefox/>

I would also like to thank the official `#general:mozilla.org` Mozilla Matrix channel, Ivan Čukić, Kai Uwe Broulik, and Lim Yuen Hoe for all their help. 
